const { _, Sequelize } = require('rappopo-sob').Helper

const schema = {
  geonameId: Sequelize.INTEGER,
  name: Sequelize.STRING,
  asciiName: Sequelize.STRING,
  fClass: Sequelize.STRING(10),
  fCode: Sequelize.STRING(20),
  lat: Sequelize.DOUBLE,
  lng: Sequelize.DOUBLE,
  admin1: { type: Sequelize.STRING(20), field: 'admin_1' },
  admin2: { type: Sequelize.STRING(100), field: 'admin_2' },
  admin3: { type: Sequelize.STRING(20), field: 'admin_3' },
  admin4: { type: Sequelize.STRING(20), field: 'admin_4' },
  country: Sequelize.STRING(2),
  pop: Sequelize.INTEGER,
  elev: Sequelize.INTEGER,
  dem: Sequelize.INTEGER,
  modDate: Sequelize.DATEONLY
}

const fields = _.concat(
  ['id'],
  _.keys(schema)
)

module.exports = ({ dbSql }) => {
  return {
    mixins: [dbSql],
    model: {
      name: 'place',
      define: schema,
      options: {
        timestamps: false,
        underscored: true,
        indexes: [
          { fields: ['geoname_id'] },
          { fields: ['name'] },
          { fields: ['ascii_name'] },
          { fields: ['lat'] },
          { fields: ['lng'] },
          { fields: ['f_code'] },
          { fields: ['f_class'] },
          { fields: ['country'] },
          { fields: ['pop'] },
          { fields: ['elev'] },
          { fields: ['dem'] }
        ]
      }
    },
    settings: {
      webApi: { readonly: true, anonymous: ['list', 'get'] },
      fields
    }
  }
}
